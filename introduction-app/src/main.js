import Vue from 'vue';
import './plugins/vuetify';
/** Any file we create need to be referenced by relative paths */
import App from './App';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  /** render is a function called to another function named create element which returns the app.
   * render: function(createElement) { return createElement(App) }
   * 
   * This is yet another way to render elements in the dom. Usually this function is not called like this
   * the refactor is naming the createElement: h, so this refactors to the next code snippet shown in
   * almost all main.js of vue apps.
   */
  render: h => h(App)
}).$mount('#app');

/** $mount tells the instance where in the dom it should attach the application. It works the same as
 * the el property declared in the instance. Two different ways to do the same operation. In the vue
 * world there is several ways to do a single thing. Always
 */