import api from '../api/imgur';
/** module design:
 * State: images as an empty array. In some point in time it will be full of the images fetched
 * from imgur with axios.
 * 
 * Getters: the only thing that can get is the list of the images stored in the state.
 * 
 * Actions: Two main functions: fetch all user images, second upload a new image to the
 * imgur service. However,  upload images does not have to call mutations because it is
 * not responsible of modifying the state of the app whatsoever. On the contrary, fetchImages
 * will modify the state calling the mutation setImages.
 * 
 * Mutations: SetImages to fill the state array of images
 */

export default {
  state: {
    images: [],
  },
  getters: {
    allImages: state => state.images,
  },
  /** Almost always mutations are simple operations, for more complex computations actions is the
   * place to do them
   */
  mutations: {
    setImages: (state, images) => state.images = images,
  },
  actions: {
    /** To access the token saved in another module of the state we can use the context.rootState,
     * all the modules are merged in a single state of truth.
     */
    fetchImages: (context) => {
      const {
        token
      } = context.rootState.auth;
      api.fetchImages(token).then(response => context.commit('setImages', response.data.data));
    },
    uploadImages: (context, images) => {
      const {
        token
      } = context.rootState.auth;
      api.upload(images, token).then(() => context.dispatch('fetchImages'));
    }
  }
};