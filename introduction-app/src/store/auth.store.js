import api from '../api/imgur';
import qs from 'qs';
import router from '../router';
/** Inside of the state we need to store the auth token as it's the core of the
 * authentication flow.
 * 
 * Getters will inform to the whole app whether or not the user is logged it.
 * The presence of token indicates if the user is logged in
 * isLoggedIn() // looke at value of token in state object
 * 
 * Mutations: setToken, very simple funcion whose intention is to update the value
 * of the token in the state, either null or a string that comes back from the
 * imgur api.
 * 
 * Actions: Flow of instructions that triggers mutations in a particular order.
 * In this example maybe login, axios to the api, recieve token, call mutation.
 * logout, in presence of a token we can logout the user to setting the token to be null
 * 
 */

export default {
  /** the initial state should check if there's a token in the localStorage, if its not, */
  state: {
    token: window.localStorage.getItem('token')
  },
  getters: {
    /** getters are called with a state (not the object of this entity). The state is passed to
     * the getters in all their calls
     * !! is a way to turn any value into a boolean
     */
    isLoggedIn: state => !!state.token
  },
  /** Mutation always are called with one or two parameters, but always at least one,
   * the state, and it might have another value, which is the value needed to update the state,
   * any type of variable, but only one, so, to set several values into the state, we send
   * an object. Mutations, as studied, are always atomic operations, also, never async.
   */
  mutations: {
    setToken: (state, token) => state.token = token
  },
  /** always is called with an object which we will call the context. This object has two
   * properties which are functions. One is commit, and commit is used to call mutations.
   * We do not call mutations like this. mutations.setToken, we call them with the commit
   * function and the name of the mutation we want to run. Commit is called with two
   * args. The first one is the name of the mutation, and the second the updated data that 
   * we want to set in the state. The second function attached in the
   */
  actions: {
    logout: (context) => {
      context.commit('setToken', null);
      localStorage.removeItem('token');
      router.push({
        name: 'home'
      });
    },
    login: () => api.login(),
    finalizeLogin: (context, hash) => {
      const queryString = qs.parse(hash.replace('#', ''));
      context.commit('setToken', queryString.access_token);
      window.localStorage.setItem('token', queryString.access_token);
      router.push({
        name: 'home'
      });
    },
  }
};

/**
 * 
https://api.imgur.com/oauth2/authorize?
client_id=YOUR_CLIENT_ID&
response_type=token
state=APPLICATION_STATE

 */