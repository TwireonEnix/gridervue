import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home';

Vue.use(Router);

/** When the auth comes back it does with all the information attached to the url
 * so we are going to use a authHandler component to extract that information.
 * This component is not going to show anything in the screen per se, but it will
 * contain the logic to reach out to the state management and save the token there
 */
export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/identicon',
      name: 'identicon',
      component: () => import('./views/Identicon')
    },
    {
      path: '/youtube',
      name: 'youtube',
      component: () => import('./views/Youtube')
    },
    {
      path: '/oauth2/callback',
      name: 'auth',
      component: () => import('./views/AuthHandler')
    },
    {
      path: '/imgur',
      name: 'imgur',
      component: () => import('./views/ImageList')
    },

    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () =>
    //     import(/* webpackChunkName: 'about' */ './views/About.vue')
    // }
  ]
});