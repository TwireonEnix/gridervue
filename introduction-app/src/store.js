import Vue from 'vue';
import Vuex from 'vuex';
import auth from './store/auth.store';
import image from './store/image.store';

Vue.use(Vuex);

/** Library to manage the flow the data across the app.
 * The previous approach in the video small app does not scale well in an environment 
 * with a lot of components that need to share tons of data. Like for example, the auth
 * flow and information. To solve this issue Vuex library is the way to store and 
 * manage that information. 
 * 
 * In this app, there must be two main modules: 
 * Auth Module: knows everything about auth
 * Image Module: knows everything about images
 * 
 * These two modules will contain all the login that belongs to that particular task.
 * There will be far less logic embedded in the components, and they will be more concerned
 * about how the information is presented to the user. These modules are like services in angular.
 * I used it the wrong way.
 * 
 * Vuex Modules:
 * Every module will contain these properties: 
 * state
 * mutations
 * actions
 * getters
 * 
 * Stephen said that it would not be suitable to give the exact definitions of what these are.
 * Instead he's going to explain them in a pragmatical example.
 * 
 * State: 
 * section that holds all the raw data of the module.
 * 
 * Getters: 
 * retrievers of the raw data from the state passing through some kind of computation,
 * They can be used to any type of operation done on the state and retrieve it. They're not
 * necessarily used as filtering operations. In general getters are set as computed properties
 * on the components that requires them. So, when a mutation makes a change in any property
 * of the state, any component using a getter that references that property is automatically
 * informed about the change, it listens to them, reacting acordingly
 * 
 * Mutations: Single step ways in which we would like to change the data saved in the state.
 * They are always atomic operations. Functions that operate over the state and modify it
 * in an individual way.
 * 
 * Actions: Functions that wrap individual actions (mutations) into a whole task. They 
 * assemble together multiple (or one) mutations in a logical order to acomplish a task. 
 * The idea is taking several mutations to perform a specific task.
 * 
 * Inside the components the store is wired through calls to the actions and getters.
 * Mutations and state are never accesed or modified by components.
 * 
 * Every time the aplication restarts in the browser, the state reverts to its initial state
 */

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    image
  }
});