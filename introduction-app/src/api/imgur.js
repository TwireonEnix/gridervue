import axios from 'axios';
axios.defaults.baseURL = 'https://api.imgur.com/';

export default {
  login() {
    window.location = `https://api.imgur.com/oauth2/authorize?
    client_id=${process.env.VUE_APP_ROOT_IMGUR_ID}&response_type=token`;
  },
  /** Problem here is that the token is sitting in a completely different module in the 
   * store.
   */
  fetchImages(token) {
    return axios.get(`3/account/me/images`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  upload(images, token) {
    /** To form an array of the object like array that comes from the input. Then mapping
     * it to extract the file binary. Each task will return a promise, saved in the array,
     * Then we save this array of promises in a constant to return one single promise that
     * will fire when all of the promises inside finish.
     */
    const promises = Array.from(images).map(image => {
      /** FormData vanilla JS allows to extract the real file from the array and be able to
       * send the binary to the api.
       */
      const formData = new FormData();
      formData.append('image', image);
      return axios.post(`3/image`, formData, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
    });
    return Promise.all(promises);
  }
};

/** To get the images we need to look up in the api docs. What we need is the get account
 * images. In that request we need to attach the access token as an authorization header.
 */